import Vue from 'vue';
import VueRouter from 'vue-router';

import AuthPage from '../views/auth-page/AuthPage.vue';
import LoginPage from '../views/login-page/LoginPage.vue';
import MainPage from '../views/main-page/MainPage.vue';
import ShoperPage from '../views/shoper-page/ShopperPage.vue';

import SellerPage from '../views/seller-page/SellerPage.vue';
import SellerForm from '../layout/sellerCabinet/SellerForm.vue';
import SellerSettings from '../layout/sellerCabinet/SellerSettings.vue';
import SellerOrders from '../layout/sellerCabinet/SellerOrders.vue';

import Seller from '../layout/auth/Seller.vue';
import Businessman from '../layout/auth/seller/Businessman.vue';
import Enterprise from '../layout/auth/seller/Enterprise.vue';
import Shopper from '../layout/auth/Shopper.vue';

Vue.use(VueRouter);

const routes = [
  { path: '/', name: MainPage, component: MainPage },
  { path: '/login', name: LoginPage, component: LoginPage },
  {
    path: '/auth',
    name: AuthPage,
    component: AuthPage,
    children: [
      {
        path: 'seller',
        name: Seller,
        component: Seller,
        children: [
          { path: 'businessman', name: Businessman, component: Businessman },
          { path: 'enterprise', name: Enterprise, component: Enterprise },
        ],
      },
      { path: 'shopper', name: Shopper, component: Shopper },
    ],
  },
  {
    path: '/seller-page',
    name: SellerPage,
    component: SellerPage,
    children: [
      { path: 'form', name: SellerForm, component: SellerForm },
      { path: 'settings', name: SellerSettings, component: SellerSettings },
      { path: 'orders-history', name: SellerOrders, component: SellerOrders },
    ],
  },
  { path: '/shopper-page', name: ShoperPage, component: ShoperPage },

];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

export default router;
