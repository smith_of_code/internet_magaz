# internet-store-frontend

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Описание файлов проекта
```
public – папка содержит общедоступные файлы, такие как html и favicon.ico. Размещенные здесь статические ресурсы будут скопированы и не упакованы в веб-пакет.
```
```$xslt
src – папка содержит исходные файлы проекта.
```
```$xslt
src/assets – папка содержит ресурсы проекта, такие как png.
```
```$xslt
src/components – папка содержит компоненты Vue.
```
```$xslt
src/App.vue – основной компонент Vue-проекта.
```
```$xslt
src/main.js – основной файл проекта, который загружает Vue-приложение.
```
